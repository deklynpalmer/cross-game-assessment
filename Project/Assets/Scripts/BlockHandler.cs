﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockHandler : MonoBehaviour
{
    [Header("Destruction Properties:")]
    public GameObject m_particleSystem;
    public GameObject m_itemDrop;

    public bool m_testme;

    public void Update()
    {
        if (m_testme)
            DestroyBlock();
    }

    // Called outside of class
    public void DestroyBlock()
    {
        GameObject particles = Instantiate(m_particleSystem, transform.position, Quaternion.Euler(new Vector3(-90.0f, 0.0f, 0.0f)));
        particles.GetComponent<ParticleSystem>().Play();

        // If the item has actually been assigned then intsantiate it
        if(m_itemDrop)
            Instantiate(m_itemDrop, transform.position, Quaternion.identity);

        Destroy(gameObject);
    }
}
