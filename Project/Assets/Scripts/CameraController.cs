﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [Header("Player:")]
    public Transform m_playerTransfrom;

    [Header("Camera Properties:")]
    public float m_moveTime = 2.0f;
    public Vector3 m_offset = new Vector3(0.0f, 0.0f, -10.0f);

    void Update()
    {
        transform.position = Vector3.Lerp(transform.position, m_playerTransfrom.position + m_offset, m_moveTime * Time.deltaTime);
    }
}
