﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerMovement : MonoBehaviour
{
    [Header("Movement Variables:")]
    public float moveSpeed = 50f;
    public float jumpSpeed = 100f;

    [Header("Physics:")]
    public float maxVelcocityX = 10.0f;
    public float m_resistance = 0.5f;

    [Header("Joystick:")]
    public Joystick joystick;
    [Range(0.0f, 1.0f)]
    public float deadZone = 0.5f;

    [Header("Condtions:")]
    public bool isGrounded = false;
    public bool hasJumped = false;
    
    private Rigidbody rb;
    private SpriteRenderer rend;
    private Animator anim;
    private PlayerAttack playerAttack;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rend = GetComponent<SpriteRenderer>();
        anim = GetComponent<Animator>();
        playerAttack = GetComponent<PlayerAttack>();
    }

    void Update()
    {
        // Movement floats
        float moveHorizontal = joystick.Horizontal * moveSpeed;

        if(moveHorizontal != 0.0f)
        {
            if(rend)
            {
                if (moveHorizontal > 0.0f)
                    rend.flipX = false;
                else
                    rend.flipX = true;
            }
        }

        else
        {
            if(isGrounded)
            {
                float xSpeed = rb.velocity.x;
                float resistance = -xSpeed * m_resistance * Time.deltaTime;
                rb.AddForce(resistance, 0.0f, 0.0f);
            }
        }
        
        // If we are on the ground and haven't jumped
        if(isGrounded && !hasJumped)
        {
            // If the joystick is in the vertical position
            if(joystick.Vertical > deadZone)
            {
                rb.AddForce(0.0f, jumpSpeed, 0.0f, ForceMode.Impulse);
                hasJumped = true;
                isGrounded = false;
            }
        }

        // Movement Vector
        Vector3 movement = new Vector3(moveHorizontal, 0.0f, 0.0f);

        // Add Force
        rb.AddForce(movement, ForceMode.Force);

        // Keep the velocity to the max on the horizontal
        if (rb.velocity.x >= maxVelcocityX)
            rb.velocity = new Vector3(maxVelcocityX, rb.velocity.y, rb.velocity.z);

        else if(rb.velocity.x <= -maxVelcocityX)
            rb.velocity = new Vector3(-maxVelcocityX, rb.velocity.y, rb.velocity.z);

        anim.SetInteger("Horizontal", (int)rb.velocity.x);
        anim.SetInteger("Vertical", (int)rb.velocity.y);
        anim.SetBool("isPunching", (playerAttack.m_attackJoystick.Vertical != 0.0f || playerAttack.m_attackJoystick.Horizontal != 0.0f));
    }

    private void OnCollisionEnter(Collision collision)
    {
        // If we collide wit the environment 
        if(collision.collider.tag == "Environment")
        {
            if (collision.transform.position.y + 1.0f < transform.position.y)
            {
                // if we aren't grounded
                if (!isGrounded)
                    isGrounded = true;

                // if we have already jumped
                if (hasJumped)
                    hasJumped = false;
            }
        }
    }
}
