﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
    [Header("Joystick:")]
    public Joystick m_attackJoystick;
    [Range(0.0f, 1.0f)]
    public float m_deadZone = 0.5f;

    [Header("Raycast Proerties:")]
    [Tooltip("Amount of blocks in x and y direction")]
    public float m_maxDistance = 3.0f;
    public LayerMask m_layerMasks;

    private PlayerMovement playerMovement;

    void Start()
    {
        playerMovement = GetComponent<PlayerMovement>();
    }

    void Update()
    {
        float m_moveHorizontal = m_attackJoystick.Horizontal;
        float m_moveVertical = m_attackJoystick.Vertical;

        Debug.Log(m_moveHorizontal + " " + m_moveVertical);

        if(m_moveHorizontal > m_deadZone || m_moveHorizontal < -m_deadZone || m_moveVertical > m_deadZone || m_moveVertical < -m_deadZone)
        {
            if (playerMovement.GetComponent<Rigidbody>().velocity == Vector3.zero)
            {
                RaycastHit hit;

                Vector3 direction = new Vector3(m_moveHorizontal, m_moveVertical, 0.0f);

                float magnitude = Vector3.Magnitude(direction);
                direction.x /= magnitude;
                direction.y /= magnitude;

                if (Physics.Raycast(transform.position, direction, out hit, m_maxDistance, m_layerMasks))
                {
                    BlockHandler handler = hit.collider.gameObject.GetComponent<BlockHandler>();
                    if (handler)
                    {
                        handler.DestroyBlock();
                    }
                }
            }
        }
    }
}
