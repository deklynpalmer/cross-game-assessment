﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeBehaviour : MonoBehaviour
{
    [Header("General:")]
    public Camera m_camera;
    public GameObject[] m_logs;
    public GameObject m_leaves;

    [Header("Raycasting:")]
    public float m_maxDistance;
    public LayerMask m_layerMask;

    void Update()
    {
        // Iterate through the logs again to check if the one beneath it doesn't exist
        for(int i = 0; i < m_logs.Length; ++i)
        {
            // if we aren't the bottom log
            if(i != 0)
            {
                // if the log we are checking exists
                if(m_logs[i])
                    // if the one underneath it doesnt exist
                    if(!m_logs[i - 1])
                    {
                        // destroy the log we are currently at
                        m_logs[i].GetComponent<BlockHandler>().DestroyBlock();
                        break;
                    }

                // if the top log doesn't exists and the leaves do
                if (!m_logs[m_logs.Length - 1] && m_leaves)
                {
                    // destroy the leaves
                    m_leaves.GetComponentInChildren<BlockHandler>().DestroyBlock();
                    Destroy(m_leaves);
                }
            }
        }
    }
}
