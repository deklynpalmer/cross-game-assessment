﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugScript : MonoBehaviour
{
    public GameObject m_player;
    private Text m_text;

    void Start()
    {
        m_text = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        if(m_text)
        {
            Rigidbody prb = m_player.GetComponent<Rigidbody>();

            if(prb)
                m_text.text = $"Player Velocity X: {prb.velocity.x}";
        }
    }
}
